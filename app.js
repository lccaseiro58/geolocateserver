var express = require("express");
var fs = require('fs');
var exec = require('child_process').exec;
var busboy = require('connect-busboy');

var app = express();
app.use(busboy());

/// Post files
app.post('/uploadImage', function(req, res){
	
	var fstream;
	req.pipe(req.busboy);
	req.busboy.on('file', function (fieldname, file, filename) {
		console.log("Receiving file: " + filename);
		fstream = fs.createWriteStream(__dirname + '/files/' + filename);
		file.pipe(fstream);
		fstream.on('close', function () {
			console.log("File saved to : " + __dirname + '/files/' + filename);
			fs.writeFile(__dirname + '/files/' + "list.txt", "1\n" + filename , function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log("The file was saved!");
				}
			});
			// path absoluto para executavel seguido dos argumentos esperados
			var geolocate = exec('/home/paulo/projects/infoportugal/GeolocateServer/Geolocator ' + __dirname + '/files/');
			var messageOut = "";
			var messageErr = "";

			geolocate.stdout.on('data', function (data) {
  				messageOut+=data;
			});

			geolocate.stderr.on('data', function (data) {
				messageErr+=data;
			});

			geolocate.on('close', function (code) {
				console.log(messageOut);
				console.log(messageErr);
				res.end(messageOut);
			});
		});
	});
});

app.post('/uploadFeatures', function(req, res){
	
	var responseString = '';

	req.on('data', function(data) {
		responseString += data;
	});

  	req.on('end', function() {
		var resultObject = JSON.parse(responseString);
		var filename = 'features.json';

		fs.writeFile(__dirname + '/files/' + filename, responseString, function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("The file was saved!");
			}
		});

		fs.writeFile(__dirname + '/files/' + "list.txt", "1\n" + filename , function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("The file was saved!");
			}
		});
		
		// path absoluto para executavel seguido dos argumentos esperados
		var geolocate = exec('/home/paulo/projects/infoportugal/GeolocateServer/Geolocator ' + __dirname + '/files/features.json');
		var messageOut = "";
		var messageErr = "";

		geolocate.stdout.on('data', function (data) {
  			messageOut+=data;
		});

		geolocate.stderr.on('data', function (data) {
			messageErr+=data;
			console.log(messageErr);
		});

		geolocate.on('close', function (code) {
			console.log(messageOut);
			console.log(messageErr);
			res.end(messageOut);
		});
	});
});


app.listen(8000);
